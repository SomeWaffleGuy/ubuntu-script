#Small script to set up a native Firefox install on Ubuntu thanks to 22.04
wget --content-disposition "https://download.mozilla.org/?product=firefox-latest-ssl&os=linux64&lang=en-US"
tar -xvf firefox*.tar.bz2
sudo mv firefox /opt/
sudo ln -s /opt/firefox/firefox /usr/local/bin/firefox
wget https://gitlab.com/SomeWaffleGuy/debscript/-/raw/master/firefox.desktop
#chmod maybe not needed? Let me know.
chmod +x firefox.desktop
mv firefox.desktop ~/.local/share/applications/
rm firefox*.tar.bz2
snap remove firefox
exit 0

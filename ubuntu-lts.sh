#/bin/sh
#Ubuntu-Script: A setup script for Ubuntu LTS versions
echo "$(tput setaf 2)$(tput bold)This script will configure a fresh install of Ubuntu to be what I consider a useable desktop.
This includes$(tput sgr 0)$(tput setaf 1)$(tput bold) NON-FREE SOFTWARE AND DRIVERS$(tput sgr 0)$(tput setaf 2)$(tput bold) and suggests software which may be subject to restrictions under local law. $(tput sgr 0)"
echo -n "$(tput setaf 2)$(tput bold)Continue? 
(y/N)$(tput sgr 0) "
read answer
if echo "$answer" | grep -iq "^y" ;then
	echo "$(tput setaf 2)$(tput bold)Checking for system updates...$(tput sgr 0)"
		sudo apt-get update
		sudo apt-get dist-upgrade
	echo "$(tput setaf 2)$(tput bold)Installing basic utilities...$(tput sgr 0)"
		sudo apt-get install ubuntu-restricted-extras unrar p7zip-full gparted gnome-weather gnome-clocks cheese gnome-tweaks gnome-shell-extensions dconf-editor git curl wget smbios-utils
	echo "$(tput setaf 2)$(tput bold)Adding PPAs...$(tput sgr 0)"
		sudo add-apt-repository -y ppa:nextcloud-devs/client
		sudo apt-get update
	echo "$(tput setaf 2)$(tput bold)Getting deb files...$(tput sgr 0)"
		#STATIC DEBS
		wget https://cdn.akamai.steamstatic.com/client/installer/steam.deb
	echo "$(tput setaf 2)$(tput bold)Installing packages...$(tput sgr 0)"
		sudo dpkg -i *.deb
		sudo apt-get --fix-broken install
		rm *.deb
		sudo apt-get install nautilus-nextcloud powertop tlp thermald
	echo "$(tput setaf 2)$(tput bold)Installing ADB$(tput sgr 0)"
		git clone https://github.com/M0Rf30/android-udev-rules.git
		sudo cp ./android-udev-rules/51-android.rules /etc/udev/rules.d/51-android.rules
		sudo chmod a+r /etc/udev/rules.d/51-android.rules
		sudo groupadd adbusers
		sudo usermod -a -G adbusers $(whoami)
		sudo systemctl restart systemd-udevd.service
		wget https://dl.google.com/android/repository/platform-tools-latest-linux.zip
		unzip platform-tools-latest-linux.zip
		sudo cp platform-tools/adb platform-tools/fastboot platform-tools/mke2fs* platform-tools/e2fsdroid /usr/local/bin
		sudo rm -rf platform-tools platform-tools-latest-linux.zip android-udev-rules
		wget https://gitlab.com/SomeWaffleGuy/debscript/-/raw/master/adb-updater.sh
		chmod +x adb-updater.sh
	echo "$(tput setaf 2)$(tput bold)Installing drivers and updating...$(tput sgr 0)"
		sudo ubuntu-drivers autoinstall
		sudo apt-get update
		sudo apt-get dist-upgrade
	echo "$(tput setaf 2)$(tput bold)Changing some GNOME settings...
These can always be changed with dconf editor$(tput sgr 0)"
		gsettings set org.gnome.desktop.interface clock-show-date 'true'
		gsettings set org.gnome.desktop.interface clock-show-weekday 'true'
		gsettings set org.gnome.desktop.interface show-battery-percentage 'true'
		gsettings set org.gnome.nautilus.preferences show-create-link 'true'
		gsettings set org.gnome.nautilus.preferences thumbnail-limit '4096'
		gsettings set org.gnome.gedit.preferences.editor bracket-matching 'false'
		gsettings set org.gnome.gedit.preferences.editor highlight-current-line 'false'
		gsettings set org.gnome.shell.extensions.dash-to-dock show-trash 'true'
		gsettings set org.gnome.shell.extensions.dash-to-dock show-apps-at-top 'true'
		gsettings set org.gnome.shell.extensions.dash-to-dock dock-fixed 'false'
		echo "$(tput setaf 2)$(tput bold)That's all folks! 
Reboot to be safe. $(tput sgr 0) "
#As root (sudo -i) run;
#echo "[org/gnome/desktop/interface]
#clock-format='12h'" >> /etc/gdm3/greeter.dconf-defaults
#dpkg-reconfigure gdm3
#
#MAKE SURE NOT TO INCLUDE COMMENTING (#)
fi
exit 0

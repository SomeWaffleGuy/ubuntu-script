# Ubuntu-Script

A script to set up the current LTS version of Ubuntu to my specifications. As the others, mostly for personal use and probably not great, but feel free to do with it what you want.


### Instructions

```
wget https://gitlab.com/SomeWaffleGuy/Ubuntu-Script/-/raw/master/ubuntu-lts.sh
chmod +x ubuntu-lts.sh
./ubuntu-lts.sh
```

### Instructions for non-snap Firefox

```
wget https://gitlab.com/SomeWaffleGuy/Ubuntu-Script/-/raw/master/firefox.sh
chmod +x firefox.sh
./firefox.sh
```
